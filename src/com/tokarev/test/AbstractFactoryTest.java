package com.tokarev.test;

public class AbstractFactoryTest {

    public static void main(String[] args) {

        AbstractFactory animalFactory = new AnimalFactory();
        AbstractFactory insectFactory = new InsectFactory();

        animalFactory.createRunable().run();
        animalFactory.createJumpable().jump();
        animalFactory.createFlyable().fly();

        System.out.println("=========");

        insectFactory.createRunable().run();
        insectFactory.createJumpable().jump();
        insectFactory.createFlyable().fly();
    }
}
