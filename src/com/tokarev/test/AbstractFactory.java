package com.tokarev.test;

public abstract class AbstractFactory {
    public abstract Runable createRunable();
    public abstract Jumpable createJumpable();
    public abstract Flyable createFlyable();
}

interface Runable {
    void run();
}
interface Jumpable {
    void jump();
}
interface Flyable {
    void fly();
}

class AnimalFactory extends AbstractFactory {

    @Override
    public Runable createRunable() {
        return new AnimalRunable();
    }

    @Override
    public Jumpable createJumpable() {
        return new AnimalJumpable();
    }

    @Override
    public Flyable createFlyable() {
        return new AnimalFlyable();
    }
}

class AnimalRunable implements Runable {
    @Override
    public void run() {
        System.out.println("Animal is running");
    }
}
class AnimalJumpable implements Jumpable {
    @Override
    public void jump() {
        System.out.println("Animal is jumping");
    }
}
class AnimalFlyable implements Flyable {
    @Override
    public void fly() {
        System.out.println("Animal is flying");
    }
}

class InsectFactory extends AbstractFactory {

    @Override
    public Runable createRunable() {
        return new InsectRunable();
    }

    @Override
    public Jumpable createJumpable() {
        return new InsectJumpable();
    }

    @Override
    public Flyable createFlyable() {
        return new InsectFlyable();
    }
}

class InsectRunable implements Runable {
    @Override
    public void run() {
        System.out.println("Insect is running");
    }
}
class InsectJumpable implements Jumpable {
    @Override
    public void jump() {
        System.out.println("Insect is jumping");
    }
}
class InsectFlyable implements Flyable {
    @Override
    public void fly() {
        System.out.println("Insect is flying");
    }
}
