package com.tokarev.test;

public class FactoryTest {

    public static void main(String[] args) {

        Factory factory = new Factory();
        AbstractDataWriter dataWriter;

        dataWriter = factory.getDataWriter("test string");
        dataWriter.write();

        dataWriter = factory.getDataWriter(100);
        dataWriter.write();
    }
}
