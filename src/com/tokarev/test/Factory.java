package com.tokarev.test;

public class Factory {

    public AbstractDataWriter getDataWriter (Object object) {

        AbstractDataWriter dataWriter = null;

        if (object instanceof String) {
            dataWriter = new StringWriter(object);
        } else if (object instanceof Integer) {
            dataWriter = new IntegerWriter(object);
        }

        return dataWriter;
    }
}

abstract class AbstractDataWriter {
    public abstract void write();
}

class StringWriter extends AbstractDataWriter {

    private String data;

    public StringWriter(Object data) {
        this.data = String.valueOf(data);
    }

    @Override
    public void write() {
        System.out.println("This is String: " + data);
    }
}

class IntegerWriter extends AbstractDataWriter {

    private Integer data;

    public IntegerWriter(Object data) {
        this.data = (Integer) data;
    }

    @Override
    public void write() {
        System.out.println("This is Integer: " + String.valueOf(data));
    }
}
